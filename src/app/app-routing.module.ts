import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComicsComponent } from './pages/comics/comics.component';
import { ComicDetailsComponent } from './pages/comic-details/comic-details.component';
import { CharactersComponent } from './pages/characters/characters.component';
import { CharacterDetailsComponent } from './pages/character-details/character-details.component';

const routes: Routes = [
  { path: '', redirectTo: '/comics', pathMatch: 'full' },
  { path: 'comics', component: ComicsComponent },
  { path: 'comics/:id', component: ComicDetailsComponent },
  { path: 'characters', component: CharactersComponent },
  { path: 'characters/:id', component: CharacterDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
