import { Image } from './Image';

export interface Comic {
  id: number;
  title: string;
  description: string;
  thumbnail: Image;
  characters: RelatedCharacters;
  images: Image[];
}

interface RelatedCharacters {
  available: number;
  collectionURI: string;
  items: RelatedCharacter[];
  returned: number;
}

interface RelatedCharacter {
  name: string;
  resourceURI: string;
}
