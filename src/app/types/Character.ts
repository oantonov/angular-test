import { Image } from './Image';

export interface Character {
  id: number;
  name: string;
  description: string;
  thumbnail: Image;
  comics: RelatedComics;
}

interface RelatedComics {
  available: number;
  collectionURI: string;
  items: RelatedComic[];
  returned: number;
}

interface RelatedComic {
  name: string;
  resourceURI: string;
}
