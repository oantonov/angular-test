import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MarvelApiService } from '../../services/marvel-api.service';
import { Character } from '../../types/Character';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.css']
})
export class CharacterDetailsComponent implements OnInit {
  public character: Character;

  constructor(
    private route: ActivatedRoute,
    private marvelApi: MarvelApiService
  ) { }

  private loadCharacter(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.marvelApi.getCharacterDetails(id).subscribe(response => {
      this.character = response.data.results.pop();
    });
  }

  ngOnInit() {
    this.loadCharacter();
  }
}
