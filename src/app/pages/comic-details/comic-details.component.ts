import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MarvelApiService } from '../../services/marvel-api.service';
import { Comic } from '../../types/Comic';

@Component({
  selector: 'app-comic-details',
  templateUrl: './comic-details.component.html',
  styleUrls: ['./comic-details.component.css']
})
export class ComicDetailsComponent implements OnInit {
  public comic: Comic;

  constructor(
    private route: ActivatedRoute,
    private marvelApi: MarvelApiService
  ) { }

  private loadComic(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.marvelApi.getComicDetails(id).subscribe(response => {
      this.comic = response.data.results.pop();
    });
  }

  ngOnInit() {
    this.loadComic();
  }
}
