import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material';

import { MarvelApiService } from '../../services/marvel-api.service';
import { Character } from '../../types/Character';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {
  public characters: Character[] = [];

  public length: number;
  public pageEvent: PageEvent;
  public pageIndex = 0;
  public pageSize = 25;
  public pageSizeOptions: number[] = [5, 10, 25, 100];

  public onChangePage(event?: PageEvent) {
    if (event) {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
      this.characters = [];
      this.loadCharacters();
    }
    return event;
  }

  constructor(private marvelApi: MarvelApiService) { }

  private loadCharacters(): void {
    this.marvelApi.getCharacters(this.pageSize, this.pageIndex * this.pageSize).subscribe(response => {
      this.characters = response.data.results;
      this.length = response.data.total;
    });
  }

  ngOnInit() {
    this.loadCharacters();
  }
}
