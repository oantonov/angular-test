import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material';

import { MarvelApiService } from '../../services/marvel-api.service';
import { Comic } from '../../types/Comic';

@Component({
  selector: 'app-comics',
  templateUrl: './comics.component.html',
  styleUrls: ['./comics.component.css']
})
export class ComicsComponent implements OnInit {
  public comics: Comic[] = [];

  public length: number;
  public pageEvent: PageEvent;
  public pageIndex = 0;
  public pageSize = 25;
  public pageSizeOptions: number[] = [5, 10, 25, 100];

  public onChangePage(event?: PageEvent) {
    if (event) {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
      this.comics = [];
      this.loadComics();
    }
    return event;
  }

  constructor(private marvelApi: MarvelApiService) { }

  private loadComics(): void {
    this.marvelApi.getComics(this.pageSize, this.pageIndex * this.pageSize).subscribe(response => {
      this.comics = response.data.results;
      this.length = response.data.total;
    });
  }

  ngOnInit() {
    this.loadComics();
  }
}
