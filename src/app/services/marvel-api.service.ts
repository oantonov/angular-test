import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MarvelApiService {
  private apiKey = '53bcacb85235c7eb75076ff07acd959f';
  private apiUrl = 'https://gateway.marvel.com:443/v1/public/';

  constructor(private http: HttpClient) { }

  public getComics(limit: number, offset: number): Observable<any> {
    return this.http.get(this.createUrl('comics', { limit, offset }));
  }

  public getComicDetails(id: number): any {
    return this.http.get(this.createUrl('comics/' + id));
  }

  public getCharacters(limit: number, offset: number): Observable<any> {
    return this.http.get(this.createUrl('characters', { limit, offset }));
  }

  public getCharacterDetails(id: number): any {
    return this.http.get(this.createUrl('characters/' + id));
  }

  private createUrl(endpoint: string, params?: any): string {
    const urlParams = new URLSearchParams(params);
    urlParams.append('apikey', this.apiKey);
    return this.apiUrl + endpoint + '?' + urlParams.toString();
  }
}
